<?php

namespace PHPSTORM_META {

    registerArgumentsSet(
        'UuidVariants',
        \Garrcomm\Uuid::VARIANT_NCS,
        \Garrcomm\Uuid::VARIANT_RFC_4122,
        \Garrcomm\Uuid::VARIANT_MICROSOFT_GUID,
        \Garrcomm\Uuid::VARIANT_RESERVED_FUTURE_USE,
    );

    registerArgumentsSet(
        'UuidNamespaces',
        \Garrcomm\Uuid::NAMESPACE_DNS,
        \Garrcomm\Uuid::NAMESPACE_URL,
        \Garrcomm\Uuid::NAMESPACE_ISO_OID,
        \Garrcomm\Uuid::NAMESPACE_X500,
        \Garrcomm\Uuid::NIL,
    );

    expectedReturnValues(
        \Garrcomm\Uuid::getVariant(),
        argumentsSet("UuidVariants")
    );
    expectedArguments(
        \Garrcomm\Uuid::newV3(),
        1,
        argumentsSet("UuidNamespaces")
    );
    expectedArguments(
        \Garrcomm\Uuid::newV5(),
        1,
        argumentsSet("UuidNamespaces")
    );
}
