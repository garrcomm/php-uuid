# PHP-UUID library

**Table of contents**

* [Introduction](#introduction)
    * [How to install](#how-to-install)
    * [Code quality](#code-quality)
* [Examples](#examples)
* [UUID versions](#uuid-versions)
    * [Why UUID v1, 3, 4 and 5 but not 2?](#why-uuid-v1-3-4-and-5-but-not-2)
    * [When to use which version?](#when-to-use-which-version)
* [Class synopsis](#class-synopsis)
* [Class constants](#constants)
* [Class methods](#methods)

***
<a id="introduction"></a>
## Introduction

This library is a single-class UUID data model for PHP, supporting UUID versions 1, 3, 4 and 5.
The reason this library has been made is that many UUID libraries are very large and contain quite some overhead.
This is meant to be a single class file handling all your UUID needs.

<a id="how-to-install"></a>
### How to install

Simply copy [src/Uuid.php](src/Uuid.php) to your project or install through `composer require garrcomm/uuid`

<a id="code-quality"></a>
### Code quality

Code quality is checked with several tools to make sure everything is working and well documented.
The following methods are in use:

* [PHP Code Sniffer](https://github.com/squizlabs/PHP_CodeSniffer)
    * Fully [PSR-12](https://www.php-fig.org/psr/psr-12/) compliant
    * Generic and some PEAR DocBlock required
* [PHPUnit](https://phpunit.de/)
    * With 100% code coverage 
* [PHPStan](https://phpstan.org/)
    * With [rule level 9](https://phpstan.org/user-guide/rule-levels)

***
<a id="examples"></a>
## Examples

```php
// Create a default UUID version 1
$uuid = Garrcomm\Uuid::newV1();
echo $uuid; // 8e8bb6e0-daee-11ed-82fa-e7446a136fc7

// Create a UUID version 1 with a specific timestamp
$uuid = Garrcomm\Uuid::newV1(strtotime('1983-08-11 16:00:00'));
echo $uuid; // 98d14000-5fc9-11c1-bacc-e7446a136fc7
echo $uuid->getDateTime()->format(DateTimeInterface::RFC3339); // 1983-08-11T16:00:00+00:00
echo $uuid->getNode(); // e7:44:6a:13:6f:c7

// Create a UUID version 1 with a specific timestamp and node
$uuid = Garrcomm\Uuid::newV1(strtotime('1983-08-11 16:00:00'), 'ab:cd:ef:01:23:45');
echo $uuid; // 98d14000-5fc9-11c1-8b40-abcdef012345

// Create instance based on an existing UUID
$uuid = new Garrcomm\Uuid('98d14000-5fc9-11c1-8b40-abcdef012345');
echo $uuid->getDateTime()->format(DateTimeInterface::RFC3339); // 1983-08-11T16:00:00+00:00

// Create a namespaced version 3 UUID
$uuid = Garrcomm\Uuid::newV3('https://www.stefanthoolen.nl/', Garrcomm\Uuid::NAMESPACE_URL);
echo $uuid; // b288873f-2f8b-366d-86f6-0e024ad3cfb6

// Create a default UUID version 4
$uuid = Garrcomm\Uuid::newV4();
echo $uuid; // efdbd62e-f38a-4e36-aac9-983634b7e78c
```

There's also JSON support built-in;
```php
$uuid = Garrcomm\Uuid::newV1();
echo json_encode($uuid, JSON_PRETTY_PRINT);
```
results in:
```json
{
    "uuid": "bf23c020-db72-11ed-99a5-842282ee5183",
    "numeric": 2.5406817441082786e+38,
    "version": 1,
    "variant": "RFC_4122",
    "timestamp": {
        "date": "2023-04-15 09:48:59.789930",
        "timezone_type": 1,
        "timezone": "+00:00"
    },
    "node": "84:22:82:ee:51:83"
}
```

***
<a id="uuid-versions"></a>
### UUID versions

<a id="why-uuid-v1-3-4-and-5-but-not-2"></a>
#### Why UUID v1, 3, 4 and 5 but not 2?

Maybe, in the future, there will be added support for UUIDv2, but there is not really a good use case for it in my humble opinion.
Because of all the data stored in the UUID, with version 2, only 64 unique IDs can be generated within a timespan of 7 minutes.

<a id="when-to-use-which-version"></a>
#### When to use which version?

It's not that v5 is better than v1. It's different. Taking security and uniqueness into consideration, this table can help you choose:

| Version      | Uniqueness | Privacy | Secure | Description |
|--------------|------------|---------|--------|-------------|
| UUID&nbsp;v1 | ⭐⭐⭐⭐⭐ | ⭐ | ⭐⭐ | Contains a timestamp and computer node. You may want to choose to keep this private, but it makes the UUID very unique. |
| UUID&nbsp;v3 | ⭐⭐ | ⭐⭐⭐⭐ | ⭐⭐⭐ | Contains a namespace and string. Shall always be the same if you use the same namespace and string. Hashing is done with [md5](https://www.php.net/manual/en/function.md5.php). |
| UUID&nbsp;v4 | ⭐⭐⭐⭐ | ⭐⭐⭐⭐⭐ | ⭐⭐⭐⭐⭐ | Completely random. Doesn't contain predictable or personal data, but there's a very small possibility the UUID exists somewhere else on the world. |
| UUID&nbsp;v5 | ⭐⭐ | ⭐⭐⭐⭐⭐ | ⭐⭐⭐ | Exactly the same as UUIDv3 but hashing is done with [sha1](https://www.php.net/manual/en/function.sha1.php). |

It's true that the star rating is not really precise, but it can help decide.

Privacy is judged by the data that could possibly be extracted from the UUID.
Also, MD5 is vulnerable ([source](https://www.kb.cert.org/vuls/id/836068)) with some use cases, so v5 is slightly more secure compared to v3 and data could, in theory, be extracted from a UUIDv3.

***

<a id="class-synopsis"></a>
## Class synopsis

```php
class Uuid implements JsonSerializable {

    /* Constants */
    public const NAMESPACE_DNS = '6ba7b810-9dad-11d1-80b4-00c04fd430c8';
    public const NAMESPACE_ISO_OID = '6ba7b812-9dad-11d1-80b4-00c04fd430c8';
    public const NAMESPACE_URL = '6ba7b811-9dad-11d1-80b4-00c04fd430c8';
    public const NAMESPACE_X500 = '6ba7b814-9dad-11d1-80b4-00c04fd430c8';
    public const NIL = '00000000-0000-0000-0000-000000000000';
    public const VARIANT_NCS = 0b0;
    public const VARIANT_RFC_4122 = 0b10;
    public const VARIANT_MICROSOFT_GUID = 0b110;
    public const VARIANT_RESERVED_FUTURE_USE = 0b1110;

    /* Methods */
    public __construct(string $data)
    public __toString(): string
    public getBinaryValue(): string
    public getDateTime(): ?DateTime
    public getFormatted(): string
    public getNode(): ?string
    public getNumericValue(): float
    public getUnixTimestamp(): ?float
    public getUuidTimestamp(): ?int
    public getVariant(): int
    public getVersion(): int
    public jsonSerialize()
    public static newV1(?float $timestamp = null, ?string $node = null): self
    public static newV3(string $nameString, string $namespace = Uuid::NIL): self
    public static newV4(): self
    public static newV5(string $nameString, string $namespace = Uuid::NIL): self
}
```

***

<a id="constants"></a>
## Constants

**Uuid::NAMESPACE_DNS**  
&nbsp;&nbsp;&nbsp;&nbsp;Predefined namespace for fully-qualified domain names as name string

**Uuid::NAMESPACE_ISO_OID**  
&nbsp;&nbsp;&nbsp;&nbsp;Predefined namespace for ISO Object Identifiers as name string

**Uuid::NAMESPACE_URL**  
&nbsp;&nbsp;&nbsp;&nbsp;Predefined namespace for URLs as name string

**Uuid::NAMESPACE_X500**  
&nbsp;&nbsp;&nbsp;&nbsp;Predefined namespace for X.500 distinguished names as name string

**Uuid::NIL**  
&nbsp;&nbsp;&nbsp;&nbsp;A `nil`/`null` namespace; totally empty, but legal.

**Uuid::VARIANT_MICROSOFT_GUID**  
&nbsp;&nbsp;&nbsp;&nbsp;The UUID variant is a Microsoft GUID.

**Uuid::VARIANT_NCS**  
&nbsp;&nbsp;&nbsp;&nbsp;The UUID variant is reserved for NCS backward compatibility.

**Uuid::VARIANT_RESERVED_FUTURE_USE**  
&nbsp;&nbsp;&nbsp;&nbsp;The UUID variant is reserved for future use.

**Uuid::VARIANT_RFC_4122**  
&nbsp;&nbsp;&nbsp;&nbsp;The UUID variant complies with the [RFC-4122](https://www.rfc-editor.org/rfc/rfc4122).


***

<a id="methods"></a>
## Methods

* [Uuid::__construct](#uuid----construct) — Returns a new UUID object
* [Uuid::__toString](#uuid----tostring) — Converts the UUID object to a string
* [Uuid::getBinaryValue](#uuid--getbinaryvalue) — Returns the UUID as 16-byte binary value
* [Uuid::getDateTime](#uuid--getdatetime) — Returns the timestamp of the UUID as DateTime object (v1 only)
* [Uuid::getFormatted](#uuid--getformatted) — Returns the UUID in Standard String Format (always 36 characters long)
* [Uuid::getNode](#uuid--getnode) — Returns the node from the UUID (v1 only)
* [Uuid::getNumericValue](#uuid--getnumericvalue) — Gets the UUID as floating point value
* [Uuid::getUnixTimestamp](#uuid--getunixtimestamp) — Returns the Unix timestamp
* [Uuid::getUuidTimestamp](#uuid--getuuidtimestamp) — Returns the UUID timestamp
* [Uuid::getVariant](#uuid--getvariant) — Returns the UUID variant
* [Uuid::getVersion](#uuid--getversion) — Returns the UUID version
* [Uuid::jsonSerialize](#uuid--jsonserialize) — Returns this object, ready to be json serialized
* [Uuid::newV1](#uuid--newv1) — Returns a new UUID object populated with a unique v1 UUID
* [Uuid::newV3](#uuid--newv3) — Returns a new UUID object populated with a v3 UUID
* [Uuid::newV4](#uuid--newv4) — Returns a new UUID object populated with a unique v4 UUID
* [Uuid::newV5](#uuid--newv5) — Returns a new UUID object populated with a v5 UUID


***

<a id="uuid----construct"></a>
## Uuid::__construct
#### Description

```php
public Uuid::__construct(string $data)
```

Returns a new UUID object

#### Parameters

* **data**  
  The data for this UUID. Three formats below are supported;
    1. Binary; 16 byte/128 bit string
    2. Canonical textual representation: `00000000-0000-0000-0000-000000000000`
    3. Represented with surrounding braces: `{00000000-0000-0000-0000-000000000000}`
    4. As uniform resource name: `urn:uuid:00000000-0000-0000-0000-000000000000`.

#### Errors/Exceptions

An InvalidArgumentException is thrown when the UUID is not formatted correctly.

***
<a id="uuid----tostring"></a>
## Uuid::__toString
#### Description

```php
public Uuid::__toString(): string
```

Converts the UUID object to a string

#### Return Values

A string formatted as `00000000-0000-0000-0000-000000000000`

***
<a id="uuid--getbinaryvalue"></a>
## Uuid::getBinaryValue
#### Description

```php
public Uuid::getBinaryValue(): string
```

Returns the UUID as 16-byte binary value

#### Return Values

A string of 16 bytes

***
<a id="uuid--getdatetime"></a>
## Uuid::getDateTime
#### Description

```php
public Uuid::getDateTime(): ?DateTime
```

Returns the timestamp of the UUID as [DateTime](https://www.php.net/manual/en/class.datetime.php) object (v1 only)

#### Return Values

The DateTime object, or null when there's no timestamp available

***
<a id="uuid--getformatted"></a>
## Uuid::getFormatted
#### Description

```php
public Uuid::getFormatted(): string
```

Returns the UUID in Standard String Format (always 36 characters long)

#### Return Values

A string formatted as `00000000-0000-0000-0000-000000000000`

***
<a id="uuid--getnode"></a>
## Uuid::getNode
#### Description

```php
public Uuid::getNode(): ?string
```

Returns the node from the UUID (v1 only)

#### Return Values

The node in `17:28:39:4a:5b:6c` format, or null when there's no node available

***
<a id="uuid--getnumericvalue"></a>
## Uuid::getNumericValue
#### Description

```php
public Uuid::getNumericValue(): float
```

Gets the UUID as floating point value

#### Return Values

Returns a 128 bit integer number

***
<a id="uuid--getunixtimestamp"></a>
## Uuid::getUnixTimestamp
#### Description

```php
public Uuid::getUnixTimestamp(): ?float
```

Returns the Unix timestamp

#### Return Values

Amount of seconds passed since 1970-01-01 00:00:00 UTC as float (microseconds included)  
or null when no timestamp is available

***
<a id="uuid--getuuidtimestamp"></a>
## Uuid::getUuidTimestamp
#### Description

```php
public Uuid::getUuidTimestamp(): ?int
```

Returns the UUID timestamp

#### Return Values

Amount of seconds passed since 1582-10-15 00:00:00 UTC in 100-nanoseconds, or null when no  
timestamp is available.

***
<a id="uuid--getvariant"></a>
## Uuid::getVariant
#### Description

```php
public Uuid::getVariant(): int
```

Returns the UUID variant

#### Return Values

An integer matching one of the Uuid::VARIANT_ constants

***
<a id="uuid--getversion"></a>
## Uuid::getVersion
#### Description

```php
public Uuid::getVersion(): int
```

Returns the UUID version

#### Return Values

The UUID version (1, 3, 4 or 5)

***
<a id="uuid--jsonserialize"></a>
## Uuid::jsonSerialize
#### Description

```php
public Uuid::jsonSerialize()
```

Returns this object, ready to be json serialized

#### Return Values

This object, ready to be json serialized

***
<a id="uuid--newv1"></a>
## Uuid::newV1
#### Description

```php
public static Uuid::newV1(?float $timestamp = null, ?string $node = null): self
```

Returns a new UUID object populated with a unique v1 UUID

#### Parameters

* **timestamp**  
  Timestamp of the UUID (null = current timestamp).
* **node**  
  Node string (null = auto detect).

#### Return Values

A UUID object containing a version 1 UUID

#### Errors/Exceptions

An InvalidArgumentException is thrown when the UUID is not formatted correctly.

***
<a id="uuid--newv3"></a>
## Uuid::newV3
#### Description

```php
public static Uuid::newV3(string $nameString, string $namespace = Uuid::NIL): self
```

Returns a new UUID object populated with a v3 UUID

#### Parameters

* **nameString**  
  Input string.
* **namespace**  
  Namespace UUID (one of the Uuid::NAMESPACE_ constants, or a custom namespace).

#### Return Values

A UUID object containing a version 3 UUID

#### Errors/Exceptions

An InvalidArgumentException is thrown when the UUID is not formatted correctly.

***
<a id="uuid--newv4"></a>
## Uuid::newV4
#### Description

```php
public static Uuid::newV4(): self
```

Returns a new UUID object populated with a unique v4 UUID

#### Return Values

A UUID object containing a version 4 UUID

***
<a id="uuid--newv5"></a>
## Uuid::newV5
#### Description

```php
public static Uuid::newV5(string $nameString, string $namespace = Uuid::NIL): self
```

Returns a new UUID object populated with a v5 UUID

#### Parameters

* **nameString**  
  Input string.
* **namespace**  
  Namespace UUID (one of the static::NAMESPACE_ constants, or a custom namespace).

#### Return Values

A UUID object containing a version 5 UUID

#### Errors/Exceptions

An InvalidArgumentException is thrown when the UUID is not formatted correctly.

