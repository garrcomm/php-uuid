#!/usr/bin/env bash
echo 'eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
              inet 37.97.250.141  netmask 255.255.255.0  broadcast 37.97.250.255
              inet6 2a01:7c8:fffa:126:5054:ff:feb0:f99c  prefixlen 64  scopeid 0x0<global>
              inet6 fe80::5054:ff:feb0:f99c  prefixlen 64  scopeid 0x20<link>
              ether 52:54:00:b0:f9:9c  txqueuelen 1000  (Ethernet)
              RX packets 13599458  bytes 1787118452 (1.6 GiB)
              RX errors 0  dropped 0  overruns 0  frame 0
              TX packets 734099  bytes 159107549 (151.7 MiB)
              TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

      lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
              inet 127.0.0.1  netmask 255.0.0.0
              inet6 ::1  prefixlen 128  scopeid 0x10<host>
              loop  txqueuelen 1000  (Local Loopback)
              RX packets 78051  bytes 12079791 (11.5 MiB)
              RX errors 0  dropped 0  overruns 0  frame 0
              TX packets 78051  bytes 12079791 (11.5 MiB)
              TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
'
