<?php

declare(strict_types=1);

namespace Tests;

use Garrcomm\Uuid;

/**
 * Tests for version 4 UUIDs
 *
 * @author  Stefan Thoolen <mail@stefanthoolen.nl>
 * @license https://creativecommons.org/licenses/by-sa/4.0/ CC-BY-SA-4.0
 * @link    https://bitbucket.org/garrcomm/uuid
 */
class Uuidv4Test extends UuidTestCase
{
    /**
     * Returns a list of v4 UUIDs to test with
     *
     * @return array{'expectedUuid':string,'binaryValue':string,'numericValue':float}[]
     */
    public function uuidDataProvider(): array
    {
        return [
            [
                'expectedUuid' => '69a28680-f115-4f8f-8938-5f142aaef9ed',
                'binaryValue' => "\x69\xa2\x86\x80\xf1\x15\x4f\x8f\x89\x38\x5f\x14\x2a\xae\xf9\xed",
                'numericValue' => 140412819707202152478868400987025965056,
            ],
            [
                'expectedUuid' => '4c4a5b63-5e91-40e6-bfb1-b819c3d77dd1',
                'binaryValue' => "\x4c\x4a\x5b\x63\x5e\x91\x40\xe6\xbf\xb1\xb8\x19\xc3\xd7\x7d\xd1",
                'numericValue' => 101407411219314392197884055618142076928,
            ],
            [
                'expectedUuid' => 'e8fc7daf-d2e9-4ca9-a1a7-d7eddb2bba7e',
                'binaryValue' => "\xe8\xfc\x7d\xaf\xd2\xe9\x4c\xa9\xa1\xa7\xd7\xed\xdb\x2b\xba\x7e",
                'numericValue' => 309691903061854065098906297938133123072,
            ],
            [
                'expectedUuid' => '5fda515b-5db4-4ae7-af95-c1eff550731f',
                'binaryValue' => "\x5f\xda\x51\x5b\x5d\xb4\x4a\xe7\xaf\x95\xc1\xef\xf5\x50\x73\x1f",
                'numericValue' => 127410230428668356393008802402144354304,
            ],
            [
                'expectedUuid' => '972203ea-718d-46fd-a937-f75d13d29dd4',
                'binaryValue' => "\x97\x22\x03\xea\x71\x8d\x46\xfd\xa9\x37\xf7\x5d\x13\xd2\x9d\xd4",
                'numericValue' => 200890044878473924901475593802552967168,
            ],
            [
                'expectedUuid' => 'a3d73aa6-dc2b-4126-8c67-681ba93c74f8',
                'binaryValue' => "\xa3\xd7\x3a\xa6\xdc\x2b\x41\x26\x8c\x67\x68\x1b\xa9\x3c\x74\xf8",
                'numericValue' => 217781696737297252590613484836606705664,
            ],
            [
                'expectedUuid' => '90c7f8ce-d3d2-4500-a831-5dcde1da634f',
                'binaryValue' => "\x90\xc7\xf8\xce\xd3\xd2\x45\x00\xa8\x31\x5d\xcd\xe1\xda\x63\x4f",
                'numericValue' => 192447144892015051453754116880329605120,
            ],
            [
                'expectedUuid' => '0dfa5fcf-e742-49d9-8f90-f48d5fc2b938',
                'binaryValue' => "\x0d\xfa\x5f\xcf\xe7\x42\x49\xd9\x8f\x90\xf4\x8d\x5f\xc2\xb9\x38",
                'numericValue' => 18579981460550776444235281473120239616,
            ],
            [
                'expectedUuid' => '5fe3307e-795f-4f04-ab2a-5555e093a24d',
                'binaryValue' => "\x5f\xe3\x30\x7e\x79\x5f\x4f\x04\xab\x2a\x55\x55\xe0\x93\xa2\x4d",
                'numericValue' => 127456294562426777805589752306877857792,
            ],
            [
                'expectedUuid' => '6a027084-5e61-4a61-aed2-6ba90f5e6d92',
                'binaryValue' => "\x6a\x02\x70\x84\x5e\x61\x4a\x61\xae\xd2\x6b\xa9\x0f\x5e\x6d\x92",
                'numericValue' => 140910834264120428680602407405432602624,
            ],
        ];
    }

    /**
     * Tests the Uuid::newV4 method
     *
     * @return void
     */
    public function testCreateStatic(): void
    {
        // Performs 10 random checks, all uuids should be unique
        $uniqueCheck = array();
        for ($i = 0; $i < 10; ++$i) {
            $uuid = Uuid::newV4();
            $this->assertNotContains($uuid->getFormatted(), $uniqueCheck, 'UUID must be unique');
            $this->assertGeneric($uuid, 4);
            $uniqueCheck[] = $uuid->getFormatted();
        }
    }

    /**
     * Tests the constructor
     *
     * @param string $expectedUuid The expected UUID.
     * @param string $binaryValue  The binary value (16 bytes).
     * @param float  $numericValue The numeric value.
     *
     * @return void
     *
     * @dataProvider uuidDataProvider
     */
    public function testConstruct(string $expectedUuid, string $binaryValue, float $numericValue): void
    {
        // Regular format
        $this->assertUuidV4(new Uuid($expectedUuid), $expectedUuid, $binaryValue, $numericValue);
        // URN format
        $this->assertUuidV4(
            new Uuid('urn:uuid:' . $expectedUuid),
            $expectedUuid,
            $binaryValue,
            $numericValue
        );
        // Surrounded with braces
        $this->assertUuidV4(new Uuid('{' . $expectedUuid . '}'), $expectedUuid, $binaryValue, $numericValue);
        // By binary value
        $this->assertUuidV4(new Uuid($binaryValue), $expectedUuid, $binaryValue, $numericValue);
    }

    /**
     * Tests a specific UUID object
     *
     * @param Uuid   $uuid         The UUID object.
     * @param string $expectedUuid The expected UUID.
     * @param string $binaryValue  The binary value (16 bytes).
     * @param float  $numericValue The numeric value.
     *
     * @return void
     */
    private function assertUuidV4(Uuid $uuid, string $expectedUuid, string $binaryValue, float $numericValue): void
    {
        $json = $this->assertGeneric($uuid, 4);
        $this->assertEquals($expectedUuid, $uuid->getFormatted());
        $this->assertEquals($binaryValue, $uuid->getBinaryValue());
        $this->assertEquals($numericValue, $uuid->getNumericValue());
        $this->assertEquals($expectedUuid, $json['uuid']);
        $this->assertEquals($numericValue, $json['numeric']);
    }
}
