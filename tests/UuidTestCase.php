<?php

declare(strict_types=1);

namespace Tests;

use Garrcomm\Uuid;
use DateTime;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

/**
 * Abstract class for testing UUIDs
 *
 * @author  Stefan Thoolen <mail@stefanthoolen.nl>
 * @license https://creativecommons.org/licenses/by-sa/4.0/ CC-BY-SA-4.0
 * @link    https://bitbucket.org/garrcomm/uuid
 */
abstract class UuidTestCase extends TestCase
{
    /**
     * Regex for a valid node
     */
    protected const REGEX_NODE = '/^[0-9a-f]{2}:[0-9a-f]{2}:[0-9a-f]{2}:[0-9a-f]{2}:[0-9a-f]{2}:[0-9a-f]{2}$/';
    /**
     * Regex for a valid UUID
     */
    protected const REGEX_UUID = '/^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$/';

    /**
     * Asserts most getters; used in multiple tests. Returns the json decoded value for further tests.
     *
     * @param Uuid    $uuid             The UUID.
     * @param integer $version          The expected UUID version.
     * @param boolean $skipVariantCheck Skips checks on the variant.
     *
     * @return array{
     *     'uuid':string,
     *     'numeric':int,
     *     'version':int,
     *     'variant':string,
     *     'timestamp':null|array{'date':string,'timezone_type':int,'timezone':string},
     *     'node':string|null
     * }
     */
    protected function assertGeneric(Uuid $uuid, int $version, bool $skipVariantCheck = false): array
    {
        // Regular getters
        $this->assertEquals($version, $uuid->getVersion(), 'UUID version must be ' . $version);
        $this->assertEquals(16, strlen($uuid->getBinaryValue()), 'Binary value length must be 16');
        $this->assertMatchesRegularExpression(static::REGEX_UUID, $uuid->getFormatted(), 'Valid UUID formatted');
        $this->assertMatchesRegularExpression(static::REGEX_UUID, (string)$uuid, 'Valid UUID string cast');
        $this->assertIsFloat($uuid->getNumericValue(), 'Has numeric value');
        if (!$skipVariantCheck && $version === 0) {
            $this->assertEquals(Uuid::VARIANT_NCS, $uuid->getVariant(), 'UUID variant must be NCS');
        } elseif (!$skipVariantCheck) {
            $this->assertEquals(Uuid::VARIANT_RFC_4122, $uuid->getVariant(), 'UUID variant must be RFC-4122');
        }
        if ($version === 1) {
            $this->assertMatchesRegularExpression(static::REGEX_NODE, $uuid->getNode(), 'Node value must match');
            $this->assertIsFloat($uuid->getUnixTimestamp(), 'Unix timestamp must be a float');
            $this->assertIsInt($uuid->getUuidTimestamp(), 'UUID timestamp must be an integer');
            $this->assertInstanceOf(DateTime::class, $uuid->getDateTime(), 'DateTime object must exist');
        } else {
            $this->assertNull($uuid->getNode(), 'Must not have a node');
            $this->assertNull($uuid->getUnixTimestamp(), 'Must not have a Unix timestamp');
            $this->assertNull($uuid->getUuidTimestamp(), 'Must not have a UUID timestamp');
            $this->assertNull($uuid->getDateTime(), 'Must not have a DateTime');
        }

        // Json values
        $json = json_decode(json_encode($uuid, JSON_THROW_ON_ERROR), true, 255, JSON_THROW_ON_ERROR);
        $this->assertMatchesRegularExpression(static::REGEX_UUID, $json['uuid'], 'Valid UUID formatted in json');
        $this->assertIsNumeric($json['numeric'], 'Has numeric value in json');
        $this->assertEquals($version, $json['version'], 'UUID version must be ' . $version . ' in json');
        if (!$skipVariantCheck && $version === 0) {
            $this->assertEquals('NCS', $json['variant'], 'Variant must be NCS in json');
        } elseif (!$skipVariantCheck) {
            $this->assertEquals('RFC_4122', $json['variant'], 'Variant must be RFC_4122 in json');
        }
        if ($version === 1) {
            $this->assertMatchesRegularExpression(static::REGEX_NODE, $json['node'], 'Node value must match in json');
            $this->assertIsArray($json['timestamp'], 'Timestamp must be an array in json');
            $this->assertArrayHasKey('date', $json['timestamp'], 'Timestamp must have date in json');
            $this->assertArrayHasKey('timezone_type', $json['timestamp'], 'Timestamp must have timezone_type in json');
            $this->assertArrayHasKey('timezone', $json['timestamp'], 'Timestamp must have timezone in json');
        } else {
            $this->assertNull($json['node'], 'Must not have a node in json');
            $this->assertNull($json['timestamp'], 'Must not have a timestamp in json');
        }

        return $json;
    }

    /**
     * Asserts that two DateTime values are equal (with delta).
     *
     * @param string $expected The expected DateTime value.
     * @param string $actual   The actual DateTime value.
     * @param float  $delta    The delta to take into consideration.
     * @param string $message  Message to show on error.
     *
     * @return void
     */
    public function assertEqualsDateTimeWithDelta(
        string $expected,
        string $actual,
        float $delta,
        string $message = ''
    ): void {
        $actualDecimalPosition = strrpos($actual, '.');
        $expectedDecimalPosition = strrpos($expected, '.');
        if ($actualDecimalPosition === false) {
            throw new InvalidArgumentException('No decimal in actual value');
        }
        if ($expectedDecimalPosition === false) {
            throw new InvalidArgumentException('No decimal in actual value');
        }

        $actualLeft = substr($actual, 0, $actualDecimalPosition);
        $expectedLeft = substr($expected, 0, $expectedDecimalPosition);
        $actualRight = '0' . substr($actual, $actualDecimalPosition);
        $expectedRight = '0' . substr($expected, $expectedDecimalPosition);

        $this->assertEquals($expectedLeft, $actualLeft, $message);
        $this->assertEqualsWithDelta((float)$expectedRight, (float)$actualRight, $delta, $message);
    }
}
