#!/usr/bin/env bash
echo '
Windows IP Configuration

   Host Name . . . . . . . . . . . . : Stefan-SurfaceBook2
   Primary Dns Suffix  . . . . . . . :
   Node Type . . . . . . . . . . . . : Hybrid
   IP Routing Enabled. . . . . . . . : No
   WINS Proxy Enabled. . . . . . . . : No
   DNS Suffix Search List. . . . . . : home

Ethernet adapter Ethernet 1:

   Connection-specific DNS Suffix  . : home
   Description . . . . . . . . . . . : Surface Ethernet Adapter
   Physical Address. . . . . . . . . : A0-4A-5E-C8-F4-80
   DHCP Enabled. . . . . . . . . . . : Yes
   Autoconfiguration Enabled . . . . : Yes
   Link-local IPv6 Address . . . . . : 1234::5678:9abc:def0:1234%10(Preferred)
   IPv4 Address. . . . . . . . . . . : 192.168.2.200(Preferred)
   Subnet Mask . . . . . . . . . . . : 255.255.255.0
   Lease Obtained. . . . . . . . . . : donderdag 13 april 2023 06:05:43
   Lease Expires . . . . . . . . . . : zaterdag 15 april 2023 15:46:37
   Default Gateway . . . . . . . . . : 192.168.2.1
   DHCP Server . . . . . . . . . . . : 192.168.2.1
   DHCPv6 IAID . . . . . . . . . . . : 765479518
   DHCPv6 Client DUID. . . . . . . . : 00-01-00-01-26-C8-C4-56-70-BC-27-A3-45-90
   DNS Servers . . . . . . . . . . . : 192.168.2.1
   NetBIOS over Tcpip. . . . . . . . : Enabled

Wireless LAN adapter WiFi:

   Media State . . . . . . . . . . . : Media disconnected
   Connection-specific DNS Suffix  . : home
   Description . . . . . . . . . . . : Marvell AVASTAR Wireless-AC Network Controller
   Physical Address. . . . . . . . . : 70-BC-10-7F-41-80
   DHCP Enabled. . . . . . . . . . . : Yes
   Autoconfiguration Enabled . . . . : Yes

Ethernet adapter Bluetooth Network Connection:

   Media State . . . . . . . . . . . : Media disconnected
   Connection-specific DNS Suffix  . :
   Description . . . . . . . . . . . : Bluetooth Device (Personal Area Network)
   Physical Address. . . . . . . . . : 70-BC-10-7F-41-72
   DHCP Enabled. . . . . . . . . . . : Yes
   Autoconfiguration Enabled . . . . : Yes
'
