<?php

declare(strict_types=1);

namespace Tests;

use Garrcomm\Uuid;
use RuntimeException;

/**
 * Tests for version 3 and 5 UUIDs
 *
 * @author  Stefan Thoolen <mail@stefanthoolen.nl>
 * @license https://creativecommons.org/licenses/by-sa/4.0/ CC-BY-SA-4.0
 * @link    https://bitbucket.org/garrcomm/uuid
 */
class Uuidv3Andv5Test extends UuidTestCase
{
    /**
     * Returns a dataset to test against
     *
     * @return array{'expectedUuid':string,'nameString':string,'namespace':string,'version':int}[]
     */
    public function uuidDataProvider(): array
    {
        return [
            [
                'expectedUuid' => 'cf378d1f-aa41-3730-9594-4bca32eaa937',
                'nameString' => 'https://www.stefanthoolen.nl',
                'namespace' => '6ba7b811-9dad-11d1-80b4-00c04fd430c8',
                'version' => 3,
            ],
            [
                'expectedUuid' => '63f7c5e1-8f98-3968-a40c-7d17d394148f',
                'nameString' => 'https://www.stefanthoolen.nl',
                'namespace' => null,
                'version' => 3,
            ],
            [
                'expectedUuid' => '49ff03ae-aac8-50b1-b26a-945572144b28',
                'nameString' => 'https://www.stefanthoolen.nl',
                'namespace' => '6ba7b811-9dad-11d1-80b4-00c04fd430c8',
                'version' => 5,
            ],
            [
                'expectedUuid' => 'a9db6cb1-f274-5f8c-bb71-cee8c8d56f57',
                'nameString' => 'https://www.stefanthoolen.nl',
                'namespace' => null,
                'version' => 5,
            ],
        ];
    }

    /**
     * Tests the newV3 and newV5 methods
     *
     * @param string      $expectedUuid The expected UUID.
     * @param string      $nameString   The name input.
     * @param string|null $namespace    The namespace, or null to test the constructor without namespace.
     * @param integer     $version      The version (3 or 5).
     *
     * @return void
     *
     * @dataProvider uuidDataProvider
     */
    public function testCreateStatic(string $expectedUuid, string $nameString, ?string $namespace, int $version): void
    {
        if ($version === 3) {
            $uuid = $namespace ? Uuid::newV3($nameString, $namespace) : Uuid::newV3($nameString);
        } elseif ($version === 5) {
            $uuid = $namespace ? Uuid::newV5($nameString, $namespace) : Uuid::newV5($nameString);
        } else {
            throw new RuntimeException('This should not happen');
        }
        $json = $this->assertGeneric($uuid, $version);
        $this->assertEquals($expectedUuid, $uuid->getFormatted());
        $this->assertEquals($json['uuid'], $uuid->getFormatted());
    }

    /**
     * Tests the constructor
     *
     * @param string      $expectedUuid The expected UUID.
     * @param string      $nameString   The name input.
     * @param string|null $namespace    The namespace, or null to test the constructor without namespace.
     * @param integer     $version      The version (3 or 5).
     *
     * @return void
     *
     * @dataProvider uuidDataProvider
     */
    public function testConstruct(string $expectedUuid, string $nameString, ?string $namespace, int $version): void
    {
        $uuid = new Uuid($expectedUuid);
        $this->assertGeneric($uuid, $version);
    }
}
