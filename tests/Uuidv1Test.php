<?php

declare(strict_types=1);

namespace Tests;

use Garrcomm\Uuid;
use InvalidArgumentException;
use ReflectionClass;

/**
 * Tests for version 1 UUIDs
 *
 * @author  Stefan Thoolen <mail@stefanthoolen.nl>
 * @license https://creativecommons.org/licenses/by-sa/4.0/ CC-BY-SA-4.0
 * @link    https://bitbucket.org/garrcomm/uuid
 */
class Uuidv1Test extends UuidTestCase
{
    /**
     * Datetime formatting for validation (should match with DateTime json serialization)
     */
    private const DATETIME_FORMATTING = 'Y-m-d H:i:s.u';

    /**
     * Returns test data for several unit tests
     *
     * @return array{'testUuid':string,'node':string,'unixTimestamp':float,'uuidTimestamp':int,'formattedDate':string}[]
     */
    public function uuidDataProvider(): array
    {
        return [
            [
                'testUuid' => '6016b3c0-91b1-1104-a8a4-1234567890ab',
                'node' => '12:34:56:78:90:ab',
                'unixTimestamp' => -4884924304.789,
                'uuidTimestamp' => 73343684952110016,
                'formattedDate' => '1815-03-16 12:34:56.789000',
            ],
            [
                'testUuid' => 'a6b57260-5fb5-11c1-9f0f-1234567890ab',
                'node' => '12:34:56:78:90:ab',
                'unixTimestamp' => 429457033.3713,
                'uuidTimestamp' => 126487498333712000,
                'formattedDate' => '1983-08-11 13:37:13.371300',
            ],
        ];
    }

    /**
     * Returns test data for node determination
     *
     * @return array{'executedCommand':string,'expectedNode':string}[]
     */
    public function nodeGeneratorDataProvider(): array
    {
        // ifconfig -a command
        $return[0]['executedCommand'] = __DIR__ . '/ifconfig-simulator.sh';
        $return[0]['expectedNode'] = '52:54:00:b0:f9:9c';

        // ip addr show command
        $return[1]['executedCommand'] = __DIR__ . '/ip-addr-simulator.sh';
        $return[1]['expectedNode'] = '52:54:00:b0:f9:9c';

        // ip addr show command
        $return[2]['executedCommand'] = __DIR__ . '/ipconfig-simulator.sh';
        $return[2]['expectedNode'] = 'a0:4a:5e:c8:f4:80';

        return $return;
    }

    /**
     * Tests a UUID with regular constructor
     *
     * @param string  $testUuid      Test UUID.
     * @param string  $node          Test node matching the UUID.
     * @param float   $unixTimestamp Test unix timestamp matching the UUID.
     * @param integer $uuidTimestamp Test UUID timestamp matching the UUID.
     * @param string  $formattedDate Test formatted date matching the UUID.
     *
     * @return void
     *
     * @dataProvider uuidDataProvider
     */
    public function testConstruct(
        string $testUuid,
        string $node,
        float $unixTimestamp,
        int $uuidTimestamp,
        string $formattedDate
    ): void {
        $uuid = new Uuid($testUuid);
        $json = $this->assertGeneric($uuid, 1);
        // The delta is because PHP has issues with float point precision
        // See also: https://www.php.net/manual/en/language.types.float.php
        $this->assertEqualsWithDelta($unixTimestamp, $uuid->getUnixTimestamp(), 0.0001, 'Unix timestamp');
        $this->assertEqualsWithDelta($uuidTimestamp, $uuid->getUuidTimestamp(), 1000, 'UUID timestamp');
        $this->assertEqualsDateTimeWithDelta(
            $formattedDate,
            $uuid->getDateTime()->format(static::DATETIME_FORMATTING),
            0.0001,
            'DateTime'
        );
        $this->assertEqualsDateTimeWithDelta($formattedDate, $json['timestamp']['date'], 0.0001, 'Json timestamp');
        $this->assertEquals($node, $uuid->getNode());
    }

    /**
     * Test without predefined values
     *
     * @return void
     */
    public function testCreateStatic(): void
    {
        $uuid = Uuid::newV1();
        $this->assertGeneric($uuid, 1);
        $this->assertGreaterThan(strtotime('YESTERDAY'), $uuid->getUnixTimestamp(), 'Unix timestamp at least');
        $this->assertLessThan(strtotime('TOMORROW'), $uuid->getUnixTimestamp(), 'Unix timestamp at most');
    }

    /**
     * Tests with a predefined timestamp
     *
     * @param string  $testUuid      Test UUID.
     * @param string  $node          Test node matching the UUID.
     * @param float   $unixTimestamp Test unix timestamp matching the UUID.
     * @param integer $uuidTimestamp Test UUID timestamp matching the UUID.
     * @param string  $formattedDate Test formatted date matching the UUID.
     *
     * @return void
     *
     * @dataProvider uuidDataProvider
     */
    public function testCreateWithTimestamp(
        string $testUuid,
        string $node,
        float $unixTimestamp,
        int $uuidTimestamp,
        string $formattedDate
    ): void {
        $uuid = Uuid::newV1($unixTimestamp);
        $json = $this->assertGeneric($uuid, 1);
        // The delta is because PHP has issues with float point precision
        // See also: https://www.php.net/manual/en/language.types.float.php
        $this->assertEqualsWithDelta($unixTimestamp, $uuid->getUnixTimestamp(), 0.0001, 'Unix timestamp');
        $this->assertEqualsWithDelta($uuidTimestamp, $uuid->getUuidTimestamp(), 1000, 'UUID timestamp');
        $this->assertEqualsDateTimeWithDelta(
            $formattedDate,
            $uuid->getDateTime()->format(static::DATETIME_FORMATTING),
            0.0001,
            'DateTime'
        );
        $this->assertEqualsDateTimeWithDelta($formattedDate, $json['timestamp']['date'], 0.0001, 'Json timestamp');
    }

    /**
     * Tests with a predefined node
     *
     * @param string $testUuid Test UUID.
     * @param string $node     Test node matching the UUID.
     *
     * @return void
     *
     * @dataProvider uuidDataProvider
     */
    public function testCreateWithNode(string $testUuid, string $node): void
    {
        $uuid = Uuid::newV1(null, $node);
        $this->assertGeneric($uuid, 1);
        $this->assertEquals($node, $uuid->getNode());
    }

    /**
     * Tests with an invalid node
     *
     * @return void
     */
    public function testCreateWithInvalidNode(): void
    {
        $this->expectException(InvalidArgumentException::class);
        Uuid::newV1(null, 'foobar');
    }

    /**
     * Generates 10 UUIDs and the node should match for them all
     *
     * @return void
     */
    public function testConsistentNode(): void
    {
        // Don't use node generators
        $reflection = new ReflectionClass(Uuid::class);
        $reflection->setStaticPropertyValue('nodeGenerators', []);

        $uuid = [];
        for ($i = 0; $i < 10; ++$i) {
            $uuid[$i] = Uuid::newV1();
            $this->assertGeneric($uuid[$i], 1);
            if ($i > 0) {
                $this->assertEquals($uuid[$i - 1]->getNode(), $uuid[$i]->getNode());
            }
        }
    }

    /**
     * Tests the node generators
     *
     * @param string $executedCommand Command used to determine the node.
     * @param string $expectedNode    Expected node result.
     *
     * @return void
     *
     * @dataProvider nodeGeneratorDataProvider
     */
    public function testNodeGenerators(string $executedCommand, string $expectedNode): void
    {
        // Use only the current command
        $reflection = new ReflectionClass(Uuid::class);
        $reflection->setStaticPropertyValue('nodeGenerators', [$executedCommand]);

        $uuid = Uuid::newV1();
        $this->assertGeneric($uuid, 1);
        $this->assertEquals($expectedNode, $uuid->getNode());
    }

    /**
     * Tests with a timestamp before 1582-10-15
     *
     * @return void
     */
    public function testLowerBoundTimestamp(): void
    {
        $timestamp = -12257781904.123; // 1581-07-26 12:34:56.123
        $this->expectException(InvalidArgumentException::class);
        Uuid::newV1($timestamp);
    }
}
