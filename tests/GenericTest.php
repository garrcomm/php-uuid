<?php

declare(strict_types=1);

namespace Tests;

use Garrcomm\Uuid;
use InvalidArgumentException;
use Garrcomm\PHPUnitHelpers\FunctionMock;
use Exception;

/**
 * Some generic UUID tests
 *
 * @author  Stefan Thoolen <mail@stefanthoolen.nl>
 * @license https://creativecommons.org/licenses/by-sa/4.0/ CC-BY-SA-4.0
 * @link    https://bitbucket.org/garrcomm/uuid
 */
class GenericTest extends UuidTestCase
{
    /**
     * This method is called after each test.
     *
     * @return void
     */
    public function tearDown(): void
    {
        FunctionMock::releaseAll();
        parent::tearDown();
    }

    /**
     * Tests the backup randomizer
     *
     * @return void
     */
    public function testBackupRandomizer(): void
    {
        // Simulate the random_bytes function fails
        FunctionMock::mock(
            'Garrcomm',
            'random_bytes',
            function () {
                throw new Exception('No randomizer found');
            }
        );

        $uuid = Uuid::newV4();
        $this->assertGeneric($uuid, 4);
    }

    /**
     * Returns a list of invalid UUIDs
     *
     * @return array{'invalidUuid':string}[]
     */
    public function invalidUuidsDataProvider(): array
    {
        return [
            ['invalidUuid' => 'foobar'],                                 // Just plain text
            ['invalidUuid' => '[a6b57260-5fb5-11c1-9f0f-1234567890ab]'], // Valid UUID but wrong encapsulation
            ['invalidUuid' => 'a6b57260-5fb5-11c1-ff0f-1234567890ab'],   // Valid UUID but unsupported variant
            ['invalidUuid' => 'a6b57260-5fb5-21c1-9f0f-1234567890ab'],   // Valid UUID but unsupported version
        ];
    }

    /**
     * Returns a list of valid UUIDs with different variants
     *
     * @return array{'inputUuid':string,'variantEnum':int,'variantString':string}[]
     */
    public function variantsDataProvider(): array
    {
        return [
            [
                'inputUuid' => 'a6b57260-5fb5-11c1-6174-1234567890ab',
                'variantEnum' => Uuid::VARIANT_NCS,
                'variantString' => 'NCS',
            ],
            [
                'inputUuid' => 'a6b57260-5fb5-11c1-9f0f-1234567890ab',
                'variantEnum' => Uuid::VARIANT_RFC_4122,
                'variantString' => 'RFC_4122',
            ],
            [
                'inputUuid' => 'a6b57260-5fb5-11c1-d6c4-1234567890ab',
                'variantEnum' => Uuid::VARIANT_MICROSOFT_GUID,
                'variantString' => 'MICROSOFT_GUID',
            ],
            [
                'inputUuid' => 'a6b57260-5fb5-11c1-ef9c-1234567890ab',
                'variantEnum' => Uuid::VARIANT_RESERVED_FUTURE_USE,
                'variantString' => 'RESERVED_FUTURE_USE',
            ],
        ];
    }

    /**
     * Tests with invalid UUIDs
     *
     * @param string $invalidUuid An invalid UUID.
     *
     * @return void
     *
     * @dataProvider invalidUuidsDataProvider
     */
    public function testInvalidUuid(string $invalidUuid): void
    {
        $this->expectException(InvalidArgumentException::class);
        new Uuid($invalidUuid);
    }

    /**
     * Tests different variants
     *
     * @param string  $inputUuid     The input UUID.
     * @param integer $variantEnum   One of the UUID_VARIANT_ constants.
     * @param string  $variantString Name of the variant in the json output.
     *
     * @return void
     *
     * @dataProvider variantsDataProvider
     */
    public function testVariants(string $inputUuid, int $variantEnum, string $variantString): void
    {
        $uuid = new Uuid($inputUuid);
        $json = $this->assertGeneric($uuid, 1, true);
        $this->assertEquals($variantEnum, $uuid->getVariant());
        $this->assertEquals($variantString, $json['variant']);
    }

    /**
     * Tests the special NIL UUID
     *
     * @return void
     */
    public function testNil(): void
    {
        $uuid = new Uuid(Uuid::NIL);
        $json = $this->assertGeneric($uuid, 0);
        $this->assertEquals(0, $uuid->getNumericValue());
        $this->assertEquals('00000000-0000-0000-0000-000000000000', $uuid->getFormatted());
        $this->assertEquals(str_repeat("\0", 16), $uuid->getBinaryValue());
        $this->assertEquals('00000000-0000-0000-0000-000000000000', $json['uuid']);
    }
}
