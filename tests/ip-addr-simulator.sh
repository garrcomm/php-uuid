#!/usr/bin/env bash
echo '1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
          link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
          inet 127.0.0.1/8 scope host lo
             valid_lft forever preferred_lft forever
          inet6 ::1/128 scope host
             valid_lft forever preferred_lft forever
      2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
          link/ether 52:54:00:b0:f9:9c brd ff:ff:ff:ff:ff:ff
          inet 37.97.250.141/24 brd 37.97.250.255 scope global noprefixroute dynamic eth0
             valid_lft 71727sec preferred_lft 71727sec
          inet6 2a01:7c8:fffa:126:5054:ff:feb0:f99c/64 scope global noprefixroute dynamic
             valid_lft 2591999sec preferred_lft 604799sec
          inet6 fe80::5054:ff:feb0:f99c/64 scope link noprefixroute
             valid_lft forever preferred_lft forever
'
