<?php

declare(strict_types=1);

namespace Garrcomm;

use DateTime;
use DateTimeZone;
use Exception;
use InvalidArgumentException;
use JsonSerializable;

/**
 * PHP-UUID library by Stefan Thoolen
 *
 * @author    Stefan Thoolen <mail@stefanthoolen.nl>
 * @copyright 2023 by Stefan Thoolen (https://www.stefanthoolen.nl/)
 * @license   https://creativecommons.org/licenses/by-sa/4.0/ CC-BY-SA-4.0
 * @link      https://bitbucket.org/garrcomm/uuid
 *
 * @see https://www.uuidtools.com/uuid-versions-explained
 * @see https://www.rfc-editor.org/rfc/rfc4122
 */
class Uuid implements JsonSerializable
{
    /**
     * A `nil`/`null` namespace; totally empty, but legal.
     */
    public const NIL = '00000000-0000-0000-0000-000000000000';
    /**
     * The UUID variant is reserved for NCS backward compatibility.
     */
    public const VARIANT_NCS = 0b0;
    /**
     * The UUID variant complies with the [RFC-4122](https://www.rfc-editor.org/rfc/rfc4122).
     */
    public const VARIANT_RFC_4122 = 0b10;
    /**
     * The UUID variant is a Microsoft GUID.
     */
    public const VARIANT_MICROSOFT_GUID = 0b110;
    /**
     * The UUID variant is reserved for future use.
     */
    public const VARIANT_RESERVED_FUTURE_USE = 0b1110;
    /**
     * Difference between Unix and UUID timestamps in 100-nano seconds.
     */
    private const TIME_DIFFERENCE = 0x1b21dd213814000;
    /**
     * Regex for valid nodes
     */
    private const REGEX_NODE
        = '[0-9a-f]{2}[:\-][0-9a-f]{2}[:\-][0-9a-f]{2}[:\-][0-9a-f]{2}[:\-][0-9a-f]{2}[:\-][0-9a-f]{2}';
    /**
     * Regex for valid UUID strings
     */
    private const REGEX_UUID = '[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}';
    /**
     * Predefined namespace for fully-qualified domain names as name string
     */
    public const NAMESPACE_DNS = '6ba7b810-9dad-11d1-80b4-00c04fd430c8';
    /**
     * Predefined namespace for URLs as name string
     */
    public const NAMESPACE_URL = '6ba7b811-9dad-11d1-80b4-00c04fd430c8';
    /**
     * Predefined namespace for ISO Object Identifiers as name string
     */
    public const NAMESPACE_ISO_OID = '6ba7b812-9dad-11d1-80b4-00c04fd430c8';
    /**
     * Predefined namespace for X.500 distinguished names as name string
     */
    public const NAMESPACE_X500 = '6ba7b814-9dad-11d1-80b4-00c04fd430c8';

    /**
     * Contains 16 bytes
     *
     * @var string[]
     */
    private array $data;

    /**
     * A list of commands that can fetch the system MAC address.
     *
     * @var string[]
     */
    private static array $nodeGenerators = ['ifconfig -a', 'ip addr show', 'ipconfig /all'];

    /**
     * Returns a new UUID object
     *
     * @param string $data The data for this UUID. Three formats below are supported;
     *                     1. Binary; 16 byte/128 bit string
     *                     2. Canonical textual representation: `00000000-0000-0000-0000-000000000000`
     *                     3. Represented with surrounding braces: `{00000000-0000-0000-0000-000000000000}`
     *                     4. As uniform resource name: `urn:uuid:00000000-0000-0000-0000-000000000000`.
     *
     * @throws InvalidArgumentException An InvalidArgumentException is thrown when the UUID is not formatted correctly.
     */
    public function __construct(string $data)
    {
        if (preg_match('/^urn:uuid:' . self::REGEX_UUID . '$/i', $data)) {
            $data = substr($data, 9);
        }
        if (preg_match('/^\{' . self::REGEX_UUID . '}$/i', $data)) {
            $data = $this->stringToUuid(substr($data, 1, -1));
        }
        if (preg_match('/^' . self::REGEX_UUID . '$/i', $data)) {
            $data = $this->stringToUuid($data);
        }
        if (strlen($data) !== 16) {
            throw new InvalidArgumentException('Data is not formatted correctly: ' . $data);
        }
        $this->data = str_split($data);

        // Validates the variant and version
        if ($data === str_repeat("\0", 16)) { // Nil UUID is allowed
            return;
        }
        $this->getVariant(); // Throws an exception for invalid input
        if (!in_array($this->getVersion(), [1, 3, 4, 5])) {
            throw new InvalidArgumentException('Only v1, 3, 4 and 5 are supported for now. Not ' . $this->getVersion());
        }
    }

    /**
     * Converts a xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx string to 16 bytes
     *
     * @param string $string The input string.
     *
     * @return string
     */
    private function stringToUuid(string $string): string
    {
        $bytes = str_split(str_replace('-', '', $string), 2);
        $data = '';
        foreach ($bytes as $byte) {
            $data .= chr((int)hexdec($byte));
        }
        return $data;
    }

    /**
     * Returns a new UUID object populated with a unique v1 UUID
     *
     * @param float|null  $timestamp Timestamp of the UUID (null = current timestamp).
     * @param string|null $node      Node string (null = auto detect).
     *
     * @return self A UUID object containing a version 1 UUID
     *
     * @throws InvalidArgumentException An InvalidArgumentException is thrown when the UUID is not formatted correctly.
     */
    public static function newV1(float $timestamp = null, string $node = null): self
    {
        // Current timestamp in microseconds, adds the difference between EPOC and 1582-10-15 00:00:00 UTC
        $time = ($timestamp ?? microtime(true)) * 10000000 + self::TIME_DIFFERENCE;
        if ($time < 0) {
            throw new InvalidArgumentException('Timestamp can\'t be before 1582-10-15 00:00:00 UTC');
        }
        $time = str_split(pack("H*", sprintf('%016x', $time)));
        $clock = str_split(self::getRandomBytes(2));

        // Validate the node
        if ($node !== null && !preg_match('/^' . self::REGEX_NODE . '$/i', $node)) {
            throw new InvalidArgumentException('Node doesn\'t match the right format');
        }
        $node = $node ? preg_replace('/[^0-9a-f]/i', '', $node) : self::generateNode();

        // Set version to UUID v1
        $time[0] = chr(ord($time[0]) & 0x0f | 0x10);
        // Set bits 6-7 to 10 (DCE 1.1, ISO/IEC 11578:1996)
        $clock[0] = chr(ord($clock[0]) & 0x3f | 0x80);

        $data = bin2hex($time[4] . $time[5] . $time[6] . $time[7])
            . '-' . bin2hex($time[2] . $time[3])
            . '-' . bin2hex($time[0] . $time[1])
            . '-' . bin2hex(implode('', $clock))
            . '-' . $node;
        return new self($data);
    }

    /**
     * Returns a new UUID object populated with a v3 UUID
     *
     * @param string $nameString Input string.
     * @param string $namespace  Namespace UUID (one of the Uuid::NAMESPACE_ constants, or a custom namespace).
     *
     * @return self A UUID object containing a version 3 UUID
     *
     * @throws InvalidArgumentException An InvalidArgumentException is thrown when the UUID is not formatted correctly.
     */
    public static function newV3(string $nameString, string $namespace = self::NIL): self
    {
        return self::newV3orv5($nameString, $namespace, 3);
    }

    /**
     * Returns a new UUID object populated with a unique v4 UUID
     *
     * @return self  A UUID object containing a version 4 UUID
     */
    public static function newV4(): self
    {
        $data = str_split(self::getRandomBytes(16));
        // Set version to UUID v4
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        // Set bits 6-7 to 10 (DCE 1.1, ISO/IEC 11578:1996)
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);

        return new self(implode('', $data));
    }

    /**
     * Returns a new UUID object populated with a v5 UUID
     *
     * @param string $nameString Input string.
     * @param string $namespace  Namespace UUID (one of the static::NAMESPACE_ constants, or a custom namespace).
     *
     * @return self  A UUID object containing a version 5 UUID
     *
     * @throws InvalidArgumentException An InvalidArgumentException is thrown when the UUID is not formatted correctly.
     */
    public static function newV5(string $nameString, string $namespace = self::NIL): self
    {
        return self::newV3orv5($nameString, $namespace, 5);
    }

    /**
     * Returns a new UUID object populated with a v3 or v5 UUID
     *
     * @param string  $nameString Input string.
     * @param string  $namespace  Namespace UUID (one of the static::NAMESPACE_ constants, or a custom namespace).
     * @param integer $version    UUID version (3 or 5).
     *
     * @return self
     */
    private static function newV3orv5(string $nameString, string $namespace, int $version): self
    {
        $namespace = new self($namespace);
        if ($version === 3) {
            $hash = md5(implode('', $namespace->data) . $nameString);
        } elseif ($version === 5) {
            $hash = sha1(implode('', $namespace->data) . $nameString);
        } else {
            // @codeCoverageIgnoreStart
            // This should never ever happen since this is a private method. But just in case, an exception is thrown.
            throw new \RuntimeException('Invalid version: ' . $version);
            // @codeCoverageIgnoreEnd
        }

        // Set the UUID version
        $version = hexdec(substr($hash, 12, 2)) & 0x0f | hexdec($version . '0');
        // Set bits 6-7 to 10 (DCE 1.1, ISO/IEC 11578:1996)
        $type = hexdec(substr($hash, 16, 2)) & 0x3f | 0x80;

        $uuid
            = substr($hash, 0, 8) . '-'
            . substr($hash, 8, 4) . '-'
            . dechex($version) . substr($hash, 14, 2) . '-'
            . dechex($type) . substr($hash, 18, 2) . '-'
            . substr($hash, 20, 12);
        return new self($uuid);
    }

    /**
     * Returns the UUID version
     *
     * @return integer The UUID version (1, 3, 4 or 5)
     */
    public function getVersion(): int
    {
        return ord($this->data[6]) >> 4;
    }

    /**
     * Returns the UUID timestamp
     *
     * @return integer|null Amount of seconds passed since 1582-10-15 00:00:00 UTC in 100-nanoseconds, or null when no
     *                      timestamp is available.
     */
    public function getUuidTimestamp(): ?int
    {
        if ($this->getVersion() !== 1) {
            return null;
        }
        $data = $this->data;
        // Fetch the correct bytes from the UUID; the substr is to remove the UUID version number (1)
        return (int)hexdec(
            substr(
                sprintf(
                    str_repeat('%02x', 8),
                    ord($data[6]),
                    ord($data[7]),
                    ord($data[4]),
                    ord($data[5]),
                    ord($data[0]),
                    ord($data[1]),
                    ord($data[2]),
                    ord($data[3])
                ),
                1
            )
        );
    }

    /**
     * Returns the Unix timestamp
     *
     * @return float|null Amount of seconds passed since 1970-01-01 00:00:00 UTC as float (microseconds included)
     *                    or null when no timestamp is available
     */
    public function getUnixTimestamp(): ?float
    {
        if ($this->getVersion() !== 1) {
            return null;
        }
        return ($this->getUuidTimestamp() - self::TIME_DIFFERENCE) / 10000000;
    }

    /**
     * Returns the timestamp of the UUID as DateTime object (v1 only)
     *
     * @return DateTime|null The [DateTime](https://www.php.net/manual/en/class.datetime.php) object, or null when
     *                       there's no timestamp available
     */
    public function getDateTime(): ?DateTime
    {
        if ($this->getVersion() !== 1) {
            return null;
        }
        $result = DateTime::createFromFormat(
            'U.u',
            sprintf('%F', $this->getUnixTimestamp()),
            new DateTimeZone('UTC')
        );

        return $result === false ? null : $result;
    }

    /**
     * Returns the node from the UUID (v1 only)
     *
     * @return string|null The node in `17:28:39:4a:5b:6c` format, or null when there's no node available
     */
    public function getNode(): ?string
    {
        if ($this->getVersion() !== 1) {
            return null;
        }
        return sprintf(
            '%02x:%02x:%02x:%02x:%02x:%02x',
            ord($this->data[10]),
            ord($this->data[11]),
            ord($this->data[12]),
            ord($this->data[13]),
            ord($this->data[14]),
            ord($this->data[15])
        );
    }

    /**
     * Returns the UUID variant
     *
     * @return integer An integer matching one of the Uuid::VARIANT_ constants
     */
    public function getVariant(): int
    {
        $value = str_pad(decbin(ord($this->data[8])), 8, '0', STR_PAD_LEFT);
        if (substr($value, 0, 1) == '0') {
            return static::VARIANT_NCS;
        }
        if (substr($value, 0, 2) == '10') {
            return static::VARIANT_RFC_4122;
        }
        if (substr($value, 0, 3) == '110') {
            return static::VARIANT_MICROSOFT_GUID;
        }
        if (substr($value, 0, 4) == '1110') {
            return static::VARIANT_RESERVED_FUTURE_USE;
        }
        throw new InvalidArgumentException('Variant unknown / invalid. Must end with "0"');
    }

    /**
     * Returns the UUID as 16-byte binary value
     *
     * @return string A string of 16 bytes
     */
    public function getBinaryValue(): string
    {
        return implode('', $this->data);
    }

    /**
     * Gets the UUID as floating point value
     *
     * @return float Returns a 128 bit integer number
     */
    public function getNumericValue(): float
    {
        return hexdec(str_replace('-', '', $this->getFormatted()));
    }

    /**
     *  Returns the UUID in Standard String Format (always 36 characters long)
     *
     * @return string A string formatted as `00000000-0000-0000-0000-000000000000`
     */
    public function getFormatted(): string
    {
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($this->getBinaryValue()), 4));
    }

    /**
     * Converts the UUID object to a string
     *
     * @return string A string formatted as `00000000-0000-0000-0000-000000000000`
     */
    public function __toString(): string
    {
        return $this->getFormatted();
    }

    /**
     * Returns the MAC address of this machine when available, or another algorithm based on some server values.
     *
     * @return string
     */
    private static function generateNode(): string
    {
        foreach (self::$nodeGenerators as $command) {
            exec($command . ' 2>&1', $output, $retValue);
            if ($retValue !== 0) {
                continue;
            }
            preg_match_all(
                '/\s(' . self::REGEX_NODE . ')\s/i',
                implode('', $output),
                $matches
            );
            foreach ($matches[1] as $match) {
                $match = str_replace('-', ':', $match);
                if (!in_array(strtolower($match), ['00:00:00:00:00:00', 'ff:ff:ff:ff:ff:ff'])) {
                    $return = str_replace(':', '', $match);
                    if (is_string($return)) {
                        return $return;
                    }
                }
            }
        }

        // No MAC address found, generating in another way
        $identityString
            = ($_SERVER['HTTP_HOST'] ?? '')
            . ($_SERVER['SERVER_SIGNATURE'] ?? '')
            . ($_SERVER['SERVER_SOFTWARE'] ?? '')
            . ($_SERVER['SERVER_NAME'] ?? '')
            . ($_SERVER['SERVER_ADDR'] ?? '')
            . ($_SERVER['SERVER_PORT'] ?? '')
            . php_uname();
        return substr(md5($identityString), 0, 12);
    }

    /**
     * Returns this object, ready to be json serialized
     *
     * @return mixed This object, ready to be json serialized
     */
    public function jsonSerialize()
    {
        switch ($this->getVariant()) {
            case static::VARIANT_NCS:
                $variant = 'NCS';
                break;
            case static::VARIANT_RFC_4122:
                $variant = 'RFC_4122';
                break;
            case static::VARIANT_MICROSOFT_GUID:
                $variant = 'MICROSOFT_GUID';
                break;
            case static::VARIANT_RESERVED_FUTURE_USE:
            default:
                $variant = 'RESERVED_FUTURE_USE';
                break;
        }

        return [
            'uuid' => $this->getFormatted(),
            'numeric' => $this->getNumericValue(),
            'version' => $this->getVersion(),
            'variant' => $variant,
            'timestamp' => $this->getDateTime(),
            'node' => $this->getNode(),
        ];
    }

    /**
     * Generates a specific amount of random bytes
     *
     * @param integer $byteCount The amount of bytes.
     *
     * @return string
     */
    private static function getRandomBytes(int $byteCount): string
    {
        if ($byteCount < 1) {
            // @codeCoverageIgnoreStart
            // This should never ever happen since this is a private method. But just in case, an exception is thrown.
            throw new \RuntimeException('Can\'t generate a negative amount of bytes');
            // @codeCoverageIgnoreEnd
        }
        try {
            $return = random_bytes($byteCount);
        } catch (Exception $exception) {
            // An exception can be thrown in case of no found randomizer
            $return = '';
            for ($i = 0; $i < $byteCount; ++$i) {
                $return .= chr(mt_rand(0, 255));
            }
        }
        return $return;
    }
}
